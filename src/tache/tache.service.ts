import { Injectable } from "@nestjs/common";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { Tache } from "./entities/tache.entity";
import { InjectRepository } from "@nestjs/typeorm";

@Injectable()
export class TacheService extends TypeOrmCrudService<Tache> {
  constructor(@InjectRepository(Tache) repo) {
    super(repo);
  }
}
