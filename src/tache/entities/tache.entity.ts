import { Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { ApiHideProperty, ApiProperty } from "@nestjs/swagger";
import { Anomalie } from "../../anomalie/entities/anomalie.entity";
import { Etape } from "../../etape/entities/etape.entity";

@Entity({ name: "Tache" })
export class Tache {

  @ApiProperty({ description: "The primary key is auto-incremented, so just in case of CREATE, just set it to 0" })
  @PrimaryGeneratedColumn({ name: "id_tache" })
  idTache: number;

  @Column({ name: "data_intervention", type: "timestamp with time zone", nullable: true })
  dateIntervention: Date;

  @OneToOne(() => Anomalie, anomalie => anomalie.tache)
  @JoinColumn({ name: "anomalie_tache" })
  anomalie: Anomalie;

  @ApiHideProperty()
  @OneToMany(() => Etape, etape => etape.tache,{ eager: true })
  etapes: Etape[];
}
