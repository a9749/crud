import { Controller } from '@nestjs/common';
import { TacheService } from './tache.service';
import { ApiTags } from "@nestjs/swagger";
import { Crud } from "@nestjsx/crud";
import { Tache } from "./entities/tache.entity";


@ApiTags("Tache")
@Crud({
  model: {
    type: Tache
  },
  params: {
    idAnomalie: {
      field: "idTache",
      primary: true,
      type: "number"
    }
  },
  query: {
    join: {
      anomalie: {
        eager: true
      }
    }
  }
})
@Controller('tache')
export class TacheController {
  constructor(public service: TacheService) {}
}
