import { Controller } from "@nestjs/common";
import { PanneService } from "./panne.service";
import { ApiTags } from "@nestjs/swagger";
import { Crud, CrudController } from "@nestjsx/crud";
import { Panne } from "./entities/panne.entity";

@ApiTags("Panne")
@Crud({
  model: {
    type: Panne
  },
  params: {
    idAnomalie: {
      field: "idAnomalie",
      primary: true,
      type: "number"
    }
  }
})
@Controller("panne")
export class PanneController implements CrudController<Panne> {
  constructor(public service: PanneService) {
  }

}