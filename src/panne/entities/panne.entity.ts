import { Column, Entity } from "typeorm";
import { Anomalie } from "../../anomalie/entities/anomalie.entity";

@Entity({name:"Panne"})
export class Panne extends Anomalie {
  @Column({ name: "cause_panne" })
  causePanne: string;
}
