import { Module } from "@nestjs/common";
import { PanneService } from "./panne.service";
import { PanneController } from "./panne.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Panne } from "./entities/panne.entity";

@Module({
  imports: [TypeOrmModule.forFeature([Panne])],
  controllers: [PanneController],
  providers: [PanneService],
  exports: [PanneService]
})
export class PanneModule {
}
