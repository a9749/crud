import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Anomalie } from "../anomalie/entities/anomalie.entity";
import { Panne } from "./entities/panne.entity";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";

@Injectable()
export class PanneService extends TypeOrmCrudService<Panne> {
  constructor(@InjectRepository(Panne) repo) {
    super(repo);
  }
}
