import { Module } from "@nestjs/common";
import { DemandeSupportService } from "./demande-support.service";
import { DemandeSupportController } from "./demande-support.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { DemandeSupport } from "./entities/demande-support.entity";

@Module({
  imports: [TypeOrmModule.forFeature([DemandeSupport])],
  controllers: [DemandeSupportController],
  providers: [DemandeSupportService],
  exports: [DemandeSupportService]
})
export class DemandeSupportModule {
}
