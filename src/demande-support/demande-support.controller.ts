import { Controller } from "@nestjs/common";
import { DemandeSupportService } from "./demande-support.service";
import { Crud, CrudController } from "@nestjsx/crud";
import { DemandeSupport } from "./entities/demande-support.entity";
import { ApiTags } from "@nestjs/swagger";


@ApiTags("Demande Support")
@Crud({
  model: {
    type: DemandeSupport
  },
  params: {
    idAnomalie: {
      field: "idAnomalie",
      primary: true,
      type: "number"
    }
  }
})
@Controller("demande-support")
export class DemandeSupportController implements CrudController<DemandeSupport> {
  constructor(public service: DemandeSupportService) {
  }

}
