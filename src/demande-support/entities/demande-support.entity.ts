import { Column, Entity } from "typeorm";
import { Anomalie } from "../../anomalie/entities/anomalie.entity";

@Entity({name:"Demande_support"})
export class DemandeSupport extends Anomalie {
  @Column({ name: "contenu_demande_support" })
  contenuDemandeSupport: string;

  @Column({ name: "contenu_reponse" })
  contenuReponse: string;
}
