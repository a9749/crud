import { Injectable } from "@nestjs/common";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { DemandeSupport } from "./entities/demande-support.entity";
import { InjectRepository } from "@nestjs/typeorm";

@Injectable()
export class DemandeSupportService extends TypeOrmCrudService<DemandeSupport> {
  constructor(@InjectRepository(DemandeSupport) repo) {
    super(repo);
  }
}
