import { ApiTags } from "@nestjs/swagger";
import { Crud, CrudController } from "@nestjsx/crud";
import { Controller } from "@nestjs/common";
import { Anomalie } from "./entities/anomalie.entity";
import { AnomalieService } from "./anomalie.service";

@ApiTags("Anomalie")
@Crud({
  model: {
    type: Anomalie
  },
  params: {
    idAnomalie: {
      field: "idAnomalie",
      primary: true,
      type: "number"
    }
  },
  query: {
    join: {
      vehicule: {
        eager: true
      }
    }
  }
})
@Controller("anomalie")
export class AnomalieController implements CrudController<Anomalie> {
  constructor(public service: AnomalieService) {
  }

}
