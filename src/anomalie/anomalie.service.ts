import { Injectable } from "@nestjs/common";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { Anomalie } from "./entities/anomalie.entity";

@Injectable()
export class AnomalieService extends TypeOrmCrudService<Anomalie> {
  constructor(@InjectRepository(Anomalie) repo) {
    super(repo);
  }
}
