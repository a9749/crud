import { Module } from '@nestjs/common';
import { AnomalieService } from './anomalie.service';
import { AnomalieController } from './anomalie.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Anomalie } from "./entities/anomalie.entity";

@Module({
  imports: [TypeOrmModule.forFeature([Anomalie])],
  controllers: [AnomalieController],
  providers: [AnomalieService],
  exports: [AnomalieService]
})
export class AnomalieModule {}
