import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { ApiHideProperty, ApiProperty } from "@nestjs/swagger";
import { IsEnum } from "class-validator";
import { StatutAnomalie } from "./statut-anomalie.enum";
import { Vehicule } from "../../vehicule/entities/vehicule.entity";
import { Tache } from "../../tache/entities/tache.entity";

@Entity({ name: "Anomalie" })
export class Anomalie {

  /* TODO
  - validation
   */

  @ApiProperty({ description: "The primary key is auto-incremented, so just in case of CREATE, just set it to 0" })
  @PrimaryGeneratedColumn({ name: "anomalie_id" })
  idAnomalie: number;


  @Column({ name: "logitude_position_vehicule", type: "float" })
  logitudePositionVehicule: number;


  @Column({ name: "latitude_position_vehicule", type: "float" })
  latitudePositionVehicule: number;


  @Column({ name: "niveau_charge_vehicule" })
  niveauChargeVehicule: number;

  @Column({ name: "temperature_vehicule" })
  temperatureVehicule: number;

  @Column({ name: "status_anomalie" })
  @IsEnum(StatutAnomalie)
  statutAnomalie: StatutAnomalie;

  @ApiHideProperty()
  @Column({ name: "data_declenchement", type: "timestamp with time zone", default: () => "CURRENT_TIMESTAMP" })
  dataDeclenchement: Date;

  @Column({ name: "data_fin", type: "timestamp with time zone", nullable: true })
  dateFin: Date;

  @ManyToOne(() => Vehicule, (vehicule) => vehicule.anomalies)
  @JoinColumn({ name: "vehicule_anomalie" })
  vehicule: Vehicule;

  @ApiHideProperty()
  @OneToOne(() => Tache, tache => tache.anomalie)
  tache: Tache;
}
