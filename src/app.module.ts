import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Anomalie } from "./anomalie/entities/anomalie.entity";
import { Vehicule } from "./vehicule/entities/vehicule.entity";
import { ObstacleModule } from "./obstacle/obstacle.module";
import { PanneModule } from "./panne/panne.module";
import { DemandeSupportModule } from "./demande-support/demande-support.module";
import { DemandeSupport } from "./demande-support/entities/demande-support.entity";
import { Panne } from "./panne/entities/panne.entity";
import { Obstacle } from "./obstacle/entities/obstacle.entity";
import { TacheModule } from "./tache/tache.module";
import { EtapeModule } from "./etape/etape.module";
import { Tache } from "./tache/entities/tache.entity";
import { AnomalieModule } from "./anomalie/anomalie.module";
import { Etape } from "./etape/entities/etape.entity";


/* TODO
  - env var
 */
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: "postgres",
      host: "autorun-database-dev2.cx0fcdpndddc.eu-west-3.rds.amazonaws.com",
      // host: "localhost",
      port: 5432,
      username: "postgres",
      // password: "postgres",
      password: "gdMHCk2pap5Zz95",
      // database: "test-production",
      database: "autorun",
      synchronize: true, // false for production
      autoLoadEntities: true,
      logging: true,
      entities: [Anomalie, Vehicule, DemandeSupport, Panne, Obstacle, Tache, Etape]
    }),
    ObstacleModule,
    PanneModule,
    DemandeSupportModule,
    TacheModule,
    EtapeModule,
    AnomalieModule
  ]
})
export class AppModule {
}
