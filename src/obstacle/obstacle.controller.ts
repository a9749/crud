import { Controller } from "@nestjs/common";
import { ObstacleService } from "./obstacle.service";
import { Crud, CrudController } from "@nestjsx/crud";
import { Obstacle } from "./entities/obstacle.entity";
import { ApiTags } from "@nestjs/swagger";

@ApiTags("Obstacle")
@Crud({
  model: {
    type: Obstacle
  },
  params: {
    idAnomalie: {
      field: "idAnomalie",
      primary: true,
      type: "number"
    }
  }
})
@Controller("obstacle")
export class ObstacleController implements CrudController<Obstacle> {
  constructor(public service: ObstacleService) {
  }

}

