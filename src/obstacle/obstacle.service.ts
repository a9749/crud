import { Injectable } from "@nestjs/common";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { Obstacle } from "./entities/obstacle.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Anomalie } from "../anomalie/entities/anomalie.entity";

@Injectable()
export class ObstacleService extends TypeOrmCrudService<Obstacle> {
  constructor(@InjectRepository(Obstacle) repo) {
    super(repo);
  }
}
