import { Column, Entity } from "typeorm";
import { Anomalie } from "../../anomalie/entities/anomalie.entity";

@Entity({name:"Obstacle"})
export class Obstacle extends Anomalie {

  @Column({ name: "nom_obstacle" })
  nomObstacle: string;

}
