import { Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { Crud } from "@nestjsx/crud";
import { Etape } from "./entities/etape.entity";
import { EtapeService } from "./etape.service";


@ApiTags("Etape")
@Crud({
  model: {
    type: Etape
  },
  params: {
    idAnomalie: {
      field: "idEtape",
      primary: true,
      type: "number"
    }
  },
  query: {
    join: {
      tache: {
        eager: true
      }
    }
  }
})
@Controller("etape")
export class EtapeController {
  constructor(public service: EtapeService) {
  }
}
