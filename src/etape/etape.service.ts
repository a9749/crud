import { Injectable } from "@nestjs/common";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { Etape } from "./entities/etape.entity";

@Injectable()
export class EtapeService extends TypeOrmCrudService<Etape> {
  constructor(@InjectRepository(Etape) repo) {
    super(repo);
  }
}
