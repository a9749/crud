import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Tache } from "../../tache/entities/tache.entity";
import { Exclude } from "class-transformer";

@Entity({ name: "Etape" })
export class Etape {

  @PrimaryGeneratedColumn({ name: "id_etape" })
  idEtape: number;

  @Column({ name: "details_etape" })
  detailsEtape: string;

  @Column({ name: "date_etape", type: "timestamp with time zone", nullable: true })
  dateEtape: Date;

  @ManyToOne(() => Tache, tache => tache.etapes,{ eager: false })
  // @Exclude({ toPlainOnly: true })
  @JoinColumn({ name: "tache_etape" })
  tache: Tache;
}
