import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Anomalie } from "../../anomalie/entities/anomalie.entity";
import { ApiHideProperty, ApiProperty } from "@nestjs/swagger";

@Entity({ name: "Vehicule", synchronize: false })
export class Vehicule {

  @ApiProperty({ description: "The primary key is auto-incremented, so in case of CREATE, just set it to 0" })
  @PrimaryGeneratedColumn({ name: "vehicule_id" })
  idVehicule: number;

  @Column({ name: "marque" })
  marque: string;

  @ApiHideProperty()
  @OneToMany(() => Anomalie, (anomalie) => anomalie.vehicule)
  anomalies: Anomalie[];

}